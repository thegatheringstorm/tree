
#ifndef TREE_TREE_H
#define TREE_TREE_H

#include <vector>
#include <memory>


template<class T>
class TreeNode {
public:

    TreeNode(T value)
            : value(value) {}

    T &getValue() { return value; }

    bool HasChild() {
        return (!children.empty());
    }

    void addChild(std::shared_ptr<TreeNode<T>> ptr) {
        children.push_back(ptr);
    }

    std::vector<std::shared_ptr<TreeNode<T>>> &getChildren() { return children; }

protected:
    T value;
    std::vector<std::shared_ptr<TreeNode<T>>> children;
};


#endif //TREE_TREE_H
