#include <iostream>
#include <vector>
#include <memory>
#include <stack>
#include "Tree.h"
#include "BinaryTree.h"
#include <assert.h>

using namespace std;

string RemoveBracesOnBothSides(string s) { return s.substr(1, s.length() - 2); }

vector<string> split(string s, char sign) {
    vector<string> result;
    string temp;
    for (auto &a:s) {
        if (a == sign) {
            result.push_back(temp);
            temp = "";
        } else temp += a;
    }
    if (temp.length() != 0) result.push_back(temp);
    return result;
}

template<class T, class A>
T join(const A &begin, const A &end, const T &t) {
    T result;
    for (A it = begin;
         it != end;
         it++) {
        if (!result.empty())
            result.append(t);
        result.append(*it);
    }
    return result;
}

bool IsTreeNodeWithoutChild(string s) {
    auto len = s.length();
    return (len == 1) || (len == 3);
}

vector<string> SplitDescriptionString(string descriptionString) {
    vector<string> temp;
    vector<string> result;
    int counter = 0;


    for (auto &element:(split(RemoveBracesOnBothSides(descriptionString), ','))) {
        if ((IsTreeNodeWithoutChild(element)) && (counter == 0)) result.push_back(element);
        else {
            if (element[0] == '{') {
                temp.push_back(element);
                counter++;
            } else if (element[element.length() - 1] == '}') {
                temp.push_back(element);
                counter--;
                if (counter == 0) {
                    result.push_back(join(temp.begin(), temp.end(), std::string(",")));
                    temp.clear();
                }
            } else {
                temp.push_back(element);
            }
        }
    }
    return result;
}


shared_ptr<TreeNode<char>> ConvertDescriptionStringOrSingleWordToTreeNode(string s) {
    if (IsTreeNodeWithoutChild(s)) {
        switch (s.length()) {
            case 1:
                return make_shared<TreeNode<char>>(s[0]);
            case 3:
                return make_shared<TreeNode<char>>(s[1]);
        }
    } else {
        vector<shared_ptr<TreeNode<char>>> temp;
        for (auto &element:SplitDescriptionString(s)) {
            temp.push_back(ConvertDescriptionStringOrSingleWordToTreeNode(element));
        }
        auto rootPtr = temp[0];
        for (int i = 1; i < temp.size(); i++) rootPtr->addChild(temp[i]);
        return rootPtr;
    }
}

shared_ptr<BinaryTreeNode<char>> ConvertForestToBinaryTreeNode(vector<shared_ptr<TreeNode<char>>> TreePtrs) {
    vector<shared_ptr<BinaryTreeNode<char>>> binaryTreePtrs;
    for (auto TreePtr:TreePtrs) {
        binaryTreePtrs.push_back(make_shared<BinaryTreeNode<char>>(TreePtr->getValue()));
        if (TreePtr->HasChild())
            (binaryTreePtrs.back())->setLeftChild(ConvertForestToBinaryTreeNode(TreePtr->getChildren()));
    }

    for (int i = 0; i < (binaryTreePtrs.size() - 1); i++) {
        binaryTreePtrs[i]->setRightChild(binaryTreePtrs[i + 1]);
    }

    return binaryTreePtrs[0];
}

vector<shared_ptr<TreeNode<char>>> ConvertDescriptionStringsToTreeNodes(string s) {
    vector<shared_ptr<TreeNode<char>>> result;
    for (auto element:split(s, '#')) result.push_back(ConvertDescriptionStringOrSingleWordToTreeNode(element));
    return result;
}


string PreorderTraversal(const shared_ptr<BinaryTreeNode<char>> &BinaryTreePtr) {
    stack<shared_ptr<BinaryTreeNode<char>>> s;
    vector<char> tempChars;
    shared_ptr<BinaryTreeNode<char>> p = BinaryTreePtr;
    while (p != nullptr || !s.empty()) {
        while (p != nullptr) {
            auto temp = (p->getValue());
            tempChars.push_back(temp);
            s.push(p);
            p = p->getLeftChild();
        }
        if (!s.empty()) {
            p = s.top();
            s.pop();
            p = p->getRightChild();
        }
    }
    return (string(tempChars.begin(), tempChars.end()));
}


string PostorderTraversal(const shared_ptr<BinaryTreeNode<char>> &root) {
    stack<shared_ptr<BinaryTreeNode<char>>> s;
    vector<char> tempChars;
    shared_ptr<BinaryTreeNode<char>> p = root;
    while (p != nullptr || !s.empty()) {
        while (p != nullptr) {
            s.push(p);
            p = p->getLeftChild();
        }
        if (!s.empty()) {
            p = s.top();
            tempChars.push_back(p->getValue());
            s.pop();
            p = p->getRightChild();
        }
    }
    return (string(tempChars.begin(), tempChars.end()));
}

void TempPostorderTraversal(const shared_ptr<BinaryTreeNode<char>> &root) {
    if (root->hasLeftChild()) TempPostorderTraversal(root->getLeftChild());
    if (root->hasLeftChild()) TempPostorderTraversal(root->getLeftChild());
    cout << root->getValue() << ' ' << root->getStart() << ' ' << root->getEnd() << endl;

}

void Code(shared_ptr<BinaryTreeNode<char>> rootPtr, int *counterPtr) {
    pair<char, pair<int, int>> temp;
    rootPtr->setStart(*counterPtr);
    (*counterPtr)++;
    if (rootPtr->hasLeftChild()) Code(rootPtr->getLeftChild(), counterPtr);
    rootPtr->setEnd(*counterPtr);
    (*counterPtr)++;
    if (rootPtr->hasRightChild()) Code(rootPtr->getRightChild(), counterPtr);
};

int main(int argc, char *argv[]) {

    string test1 = "{a,{c,{s,f},d,e},b,{s,f},{s,f}}#{a,{b,d,e},c}";
    auto test1Result = ConvertDescriptionStringsToTreeNodes(test1);
    assert(test1Result[1]->getChildren()[0]->getChildren()[1]->getValue() == 'e');

    string test2 = "{a,{b,d,e},{c,f}}";
    auto test2Result = ConvertDescriptionStringsToTreeNodes(test2);
    auto test2Result2 = ConvertForestToBinaryTreeNode(test2Result);
    auto test2Result3 = PreorderTraversal(test2Result2);
    assert(test2Result3 == "abdecf");

    string test3 = "{a,{b,d,e},c}";
    auto test3Result = ConvertDescriptionStringsToTreeNodes(test3);
    auto test3Result2 = ConvertForestToBinaryTreeNode(test3Result);
    auto test3Result3 = PostorderTraversal(test3Result2);
    assert(test3Result3 == "debca");

    int a = 0;
    int *b = &a;
    Code(test3Result2, b);
    TempPostorderTraversal(test3Result2);

    return 0;
}