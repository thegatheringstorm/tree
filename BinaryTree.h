//
// Created by tgs on 17-11-25.
//

#ifndef TREE_BINARYTREE_H
#define TREE_BINARYTREE_H

#include <memory>


template<class T>
class BinaryTreeNode {
public:

    BinaryTreeNode(T
                   value)
            :
            value(value) {}

    T &getValue() { return value; }

    bool HasChild() {
        return (leftChild || rightChild);
    }


    void setRightChild(std::shared_ptr<BinaryTreeNode<T>> ptr) {
        rightChild = ptr;
    }

    void setLeftChild(std::shared_ptr<BinaryTreeNode<T>> ptr) {
        leftChild = ptr;
    }

    bool hasRightChild(void) {
        if (getRightChild() != nullptr) return true;
        else return false;
    }


    bool hasLeftChild(void) {
        if (getLeftChild() != nullptr) return true;
        else return false;
    }

    std::shared_ptr<BinaryTreeNode<T>> getRightChild(void) { return rightChild; }

    std::shared_ptr<BinaryTreeNode<T>> getLeftChild(void) { return leftChild; }

    void clearTree(void) {
        setLeftChild(nullptr);
        setRightChild(nullptr);
    }

    void setStart(int x) {
        start = x;
    }

    void setEnd(int x) {
        end = x;
    }

    int getStart() { return start; }

    int getEnd() { return end; }


protected:
    T value;
    int start;
    int end;
    std::shared_ptr<BinaryTreeNode<T>> leftChild;
    std::shared_ptr<BinaryTreeNode<T>> rightChild;
};


#endif //TREE_BINARYTREE_H
